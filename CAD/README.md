The clamp plates image specifies which parameters relate to which dimensions of the clamp unit.

The hole spacing and diameters should be measured from whichever fan unit is being used.
The airflow hole size will control the area of contact that the filter has and the area that air is able to go through.

The filter thickness parameter must be selected carefully, the clamp should aim to hold the filter in place with friction therefore tf must be made smaller than the actual measure filter thickness. it is also possible to layer filter paper to achieve a greater thickness.
