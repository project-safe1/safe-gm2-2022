//Customizeable parameters
h = 105;
p1 = 20;
r1 = 50;
t = 6;
d1 = 4;
f = 100;
tf = 4;

d2 = 8;
p2 = 6;

//Derived parameters
mount_screw_radius = d1/2;
thumb_screw_radius = d2/2;
width = h + p1;

difference()
{
    cube([width, width, t],center = true);
    
    //4 holes to fasten to fan
    translate([h/2,h/2,0])
    cylinder(t+0.1,mount_screw_radius,mount_screw_radius,center = true);
    
    translate([-h/2,h/2,0])
    cylinder(t+0.1,mount_screw_radius,mount_screw_radius,center = true);
    
    translate([h/2,-h/2,0])
    cylinder(t+0.1,mount_screw_radius,mount_screw_radius,center = true);
    
    translate([-h/2,-h/2,0])
    cylinder(t+0.1,mount_screw_radius,mount_screw_radius,center = true);
    
   
    //Slot for filter
    translate([0,0,t/2])
    cube([f,f,tf],center = true);
    
    //Hole for air flow
    intersection()
    {   
    cube([f,f,t+0.1],center = true);
        cylinder(t+0.1,r1,r1,center = true);
    }
    
}

fastener_gap = h/2 + p1/2 + thumb_screw_radius + p2-3;
connector_width = thumb_screw_radius + p2;

difference(){
    union() {
    translate([0,fastener_gap,0])
    cylinder( h = t, r = connector_width,center = true);
    translate([0,fastener_gap-connector_width/2,0])
    cube([connector_width*2,connector_width+0.1,t],center=true);

    translate([0,-fastener_gap,0])
    cylinder( h = t, r = thumb_screw_radius + p2,center = true);
    translate([0,-fastener_gap+connector_width/2,0])
    cube([connector_width*2,connector_width+0.1,t],center=true);
    }
    translate([0,fastener_gap,0])
    cylinder(h=t+0.1,r = thumb_screw_radius,center = true);
    translate([0,-fastener_gap,0])
    cylinder(h=t+0.1,r = thumb_screw_radius,center = true);
}